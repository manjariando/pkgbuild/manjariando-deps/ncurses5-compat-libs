# Maintainer:  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Maintainer: Mateusz Gozdek <mgozdekof@gmail.com>
# Contributor: Allan McRae <allan@archlinux.org>
# Contributor: judd <jvinet@zeroflux.org>

pkgname=ncurses5-compat-libs
_pkgname=ncurses
pkgver=6.3
pkgrel=2.2
pkgdesc='System V Release 4.0 curses emulation library, ABI 5'
arch=(i686 x86_64)
url='http://invisible-island.net/ncurses/ncurses.html'
license=(MIT)
depends=(glibc gcc-libs sh)
provides=(libtinfo5)
conflicts=(libtinfo5)
source=("https://invisible-mirror.net/archives/${_pkgname}/${_pkgname}-${pkgver}.tar.gz"{,.asc}
        "ncurses-6.3-libs.patch" "ncurses-6.3-pkgconfig.patch")
sha256sums=('97fc51ac2b085d4cde31ef4d2c3122c21abc217e9090a43a30fc5ec21684e059'
            'SKIP'
            'dc4261b6642058a9df1c0945e2409b24f84673ddc3a665d8a15ed3580e51ee25'
            'b8544a607dfbeffaba2b087f03b57ed1fa81286afca25df65f61b04b5f3b3738')
validpgpkeys=('19882D92DDA4C400C22C0D56CC2AF4472167BE03')  # Thomas Dickey <dickey@invisible-island.net>

prepare() {
  cd ${_pkgname}-$pkgver
  # do not link against test libraries
  patch -Np1 -i ../"${_pkgname}-6.3-libs.patch"
  # do not leak build-time LDFLAGS into the pkgconfig files:
  # https://bugs.archlinux.org/task/68523
  patch -Np1 -i ../"${_pkgname}-6.3-pkgconfig.patch"
  # NOTE: can't run autoreconf because the autotools setup is custom and ancient
}

build() {
  cd ${_pkgname}-${pkgver}

  ./configure \
    --prefix=/usr \
    --enable-widec \
    --disable-pc-files \
    --mandir=/usr/share/man \
    --with-cxx-binding \
    --with-cxx-shared \
    --with-pkg-config-libdir=/usr/lib/pkgconfig \
    --with-shared \
    --with-versioned-syms \
    --without-ada \
    --without-debug \
    --with-abi-version=5
  make
}

package() {
  cd ${_pkgname}-${pkgver}
  make DESTDIR="$pkgdir" install.libs
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  rm -rf "$pkgdir/usr/include/" "$pkgdir/usr/lib/pkgconfig" \
    "$pkgdir"/usr/lib/*.so "$pkgdir"/usr/lib/*.a

  for lib in ncurses ncurses++ form panel menu; do
    ln -s /usr/lib/lib${lib}w.so.5 "$pkgdir/usr/lib/lib${lib}.so.5"
  done
  ln -s /usr/lib/libncurses.so.5 "$pkgdir/usr/lib/libtinfo.so.5"
  ln -s /usr/lib/libncurses.so.5 "$pkgdir/usr/lib/libtic.so.5"
}
